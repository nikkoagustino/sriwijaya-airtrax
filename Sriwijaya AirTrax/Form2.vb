﻿Imports MySql.Data.MySqlClient
Imports System.Security.Cryptography
Imports System.Text
Imports System.Configuration
Imports System.Globalization
Imports System.Threading
Imports System.Net

Public Class frmLogin
    Dim conn As MySqlConnection
    Dim querystring As String
    Dim result As MySqlDataReader
    Dim query As MySqlCommand
    Public pilotid As String
    Dim cfg = ConfigurationManager.AppSettings
    Dim setcfg As AppConfig

    Public Sub New()
        Dim myCi = New CultureInfo("en-US", False)
        myCi.NumberFormat.CurrencyDecimalSeparator = "."
        myCi.NumberFormat.CurrencyGroupSeparator = ","

        Thread.CurrentThread.CurrentCulture = myCi
        Thread.CurrentThread.CurrentUICulture = myCi

        InitializeComponent()
    End Sub

    Function MD5Hash(ByVal password As String)
        Dim md5 As MD5 = New MD5CryptoServiceProvider()
        Dim result As Byte()
        result = md5.ComputeHash(Encoding.ASCII.GetBytes(password))

        Dim strBuilder As New StringBuilder()

        For i As Integer = 0 To result.Length - 1
            strBuilder.Append(result(i).ToString("x2"))
        Next

        Return strBuilder.ToString()
    End Function

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        '==================== Save password =====================
        If chkSave.Checked = True Then
            Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            Dim savepilot As String = txtPilotID.Text
            Dim savepass As String = txtPassword.Text
            AppConfig.UpdateAppSettings("savedpilot", savepilot)
            AppConfig.UpdateAppSettings("savedpassword", savepass)
        End If

        Try
            Dim serv As New Encryption
            conn = New MySqlConnection()
            conn.ConnectionString = serv.Decrypt(cfg("server"))
            conn.Open()
            '================== LOGIN =======================
            If String.IsNullOrEmpty(txtPilotID.Text) Then
                MessageBox.Show("Please Fill Pilot ID")
                txtPilotID.Focus()
            ElseIf String.IsNullOrEmpty(txtPassword.Text) Then
                MessageBox.Show("Please Fill Password Field")
                txtPassword.Focus()
            Else
                pilotid = Convert.ToInt32(txtPilotID.Text.Substring(3, 4)).ToString
                lblPilotID.Text = pilotid.ToString
                If txtPilotID.Text.Substring(0, 3) <> "SJY" Then
                    MessageBox.Show("Pilot ID Not Found")
                Else
                    querystring = "SELECT * FROM phpvms_pilots WHERE pilotid = '" & pilotid & "'"
                    query = New MySqlCommand(querystring, conn)
                    result = query.ExecuteReader()
                    result.Read()
                    If result.HasRows = True Then
                        Dim accept As String = result("confirmed").ToString
                        Dim salt As String = result("salt").ToString
                        Dim pass As String = result("password").ToString
                        Dim password As String = txtPassword.Text + salt
                        Dim hash As String = MD5Hash(password)
                        If accept = "0" Then
                            MessageBox.Show("Your Account Is Not Yet Activated or Suspended. Please Contact Staff.")
                        Else
                            If String.Equals(hash, pass) Then
                                BackgroundWorker1.RunWorkerAsync()
                                frmFlopsCenter.Show()
                                Me.Hide()
                            Else
                                MessageBox.Show("Password Authentication Failed")
                            End If
                            conn.Close()
                        End If
                    Else
                        MessageBox.Show("Pilot ID Not Found")
                    End If
                    result.Close()
                End If
            End If
        Catch ex As MySqlException
            conn.Close()
            conn.Open()
            MessageBox.Show("Connection Failed. Please Try Again.")
        End Try

    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Application.CurrentCulture = New CultureInfo("EN-US")
        With txtPilotID
            .Text = cfg("savedpilot")
            .Focus()
        End With
        If cfg("savedpilot") <> "" Then
            chkSave.Checked = True
        End If
        txtPassword.Text = cfg("savedpassword")
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        AppConfig.UpdateAppSettings("savedpilot", "")
        AppConfig.UpdateAppSettings("savedpassword", "")
        chkSave.Checked = False
        With txtPilotID
            .Clear()
            .Focus()
        End With
        txtPassword.Clear()
    End Sub

    Private Sub txtPilotID_TextChanged(sender As Object, e As EventArgs) Handles txtPilotID.TextChanged
        chkSave.Checked = False
        txtPassword.Clear()
    End Sub

    Private Sub txtPassword_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPassword.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then
            btnLogin.PerformClick()
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            Dim ip As String = "0.0.0.0"
            Dim wc As New WebClient
            wc.Proxy = Nothing
            ip = wc.DownloadString("http://sriwijayavirtual.net/cekip.php")
            Dim curdate As String = DateTime.Now.ToString("yyyy-MM-dd")
            result.Close()
            Dim cmd As MySqlCommand
            cmd = New MySqlCommand("UPDATE phpvms_pilots SET lastlogin = '" & curdate & "', lastip = '" & ip & "' WHERE pilotid = '" & pilotid & "'", conn)
            cmd.ExecuteScalar()
        Catch ex As Exception
            BackgroundWorker1.CancelAsync()
            BackgroundWorker1.RunWorkerAsync()
        End Try
    End Sub
End Class