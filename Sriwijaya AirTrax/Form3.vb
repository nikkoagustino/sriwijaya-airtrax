﻿Imports MySql.Data.MySqlClient
Imports System.Configuration
Imports System.Globalization
Imports System.Threading
Imports System.Text

Public Class frmFlopsCenter

    Dim conn As MySqlConnection
    Dim querystring As String
    Dim result As MySqlDataReader
    Dim cmd As MySqlCommand
    Dim cfg = ConfigurationManager.AppSettings

    Public Sub New()
        Dim myCi = New CultureInfo("en-US", False)
        myCi.NumberFormat.CurrencyDecimalSeparator = "."
        myCi.NumberFormat.CurrencyGroupSeparator = ","

        Thread.CurrentThread.CurrentCulture = myCi
        Thread.CurrentThread.CurrentUICulture = myCi

        InitializeComponent()
    End Sub

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click
        If txtAltn.Text = "" Then
            MessageBox.Show("Please Fill 4 Letter ICAO Code Of Alternate Airport")
        ElseIf cboAircraft.SelectedIndex = -1 Then
            MessageBox.Show("Please Select Aircraft From Dropdown List")
        Else
            Try
                If conn.State = ConnectionState.Open Then
                    '==================== Transfer Departure Airport ================
                    cmd = New MySqlCommand("SELECT name FROM phpvms_airports WHERE icao = '" & txtDep.Text & "'", conn)
                    result = cmd.ExecuteReader()
                    result.Read()
                    frmFlightLog.dep = txtDep.Text
                    frmFlightLog.depfull = result("name").ToString
                    result.Close()

                    '==================== Transfer Arrival Airport ================
                    cmd = New MySqlCommand("SELECT name FROM phpvms_airports WHERE icao = '" & txtArr.Text & "'", conn)
                    result = cmd.ExecuteReader()
                    result.Read()
                    frmFlightLog.arr = txtArr.Text
                    frmFlightLog.arrfull = result("name").ToString
                    result.Close()

                    '====================== Transfer Aircraft ======================
                    Dim accombo As String = cboAircraft.SelectedItem.ToString
                    Dim acreg As String = accombo.Substring(accombo.IndexOf(" ") + 1, 6)
                    cmd = New MySqlCommand("SELECT * FROM phpvms_aircraft WHERE registration = '" & acreg & "'", conn)
                    result = cmd.ExecuteReader()
                    result.Read()
                    frmFlightLog.acreg = result("registration").ToString
                    frmFlightLog.actype = result("fullname").ToString
                    frmFlightLog.ac_id = result("id").ToString
                    result.Close()
                Else
                    conn.Close()
                    conn.Open()
                    MessageBox.Show("Connection Failed. Please Try Again.")
                    cmdSubmit.PerformClick()
                End If

                '==================== Transfer Other Value ===================
                frmFlightLog.fltno = txtFltNo.Text
                frmFlightLog.route = txtRoute.Text
                frmFlightLog.cruising = txtCrz.Text

                '======================= Transfer Operator ====================
                If txtFltNo.Text.Substring(0, 3) = "SJY" Then
                    If txtDep.Text.Substring(0, 2) = "WM" Or txtArr.Text.Substring(0, 2) = "WM" Then
                        frmFlightLog.opr = "Operated by You Wings"
                    Else
                        frmFlightLog.opr = "Operated by Sriwijaya Air"
                    End If
                ElseIf txtFltNo.Text.Substring(0, 3) = "NIH" Then
                    frmFlightLog.opr = "Operated by NAM Air"
                End If

                '==================== Show Close Form ================
                frmFlightLog.Show()
                Me.Hide()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub cboFlightBid_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFlightBid.SelectedIndexChanged
        Try
            Dim combo As String = cboFlightBid.SelectedItem.ToString
            Dim bidid As String = combo.Substring(0, combo.IndexOf(":"))
            Dim code As String = combo.Substring(combo.LastIndexOf(": ") + 2, 3)

            '================== Filling Flightplan ===================
            cmd = New MySqlCommand("SELECT * FROM phpvms_schedules WHERE bidid = '" & bidid & "'", conn)
            result = cmd.ExecuteReader()
            result.Read()
            txtDep.Text = result("depicao").ToString
            txtArr.Text = result("arricao").ToString
            txtFltNo.Text = result("code").ToString & result("flightnum").ToString
            txtRoute.Text = result("route").ToString
            txtEET.Text = result("flighttime").ToString
            txtCrz.Text = result("flightlevel").ToString
            frmFlightLog.routeid = result("id").ToString
            frmFlightLog.bidid = result("bidid").ToString
            cboAircraft.SelectedIndex = -1
            With txtAltn
                .Clear()
                .Focus()
            End With
            result.Close()

            '==================== Aircraft selection =================
            If code = "SJY" Then
                cmd = New MySqlCommand("SELECT * FROM phpvms_aircraft WHERE registration LIKE 'PK-C%' AND enabled = 1 ORDER BY icao ASC", conn)
            ElseIf code = "NIH" Then
                cmd = New MySqlCommand("SELECT * FROM phpvms_aircraft WHERE registration LIKE 'PK-N%' AND enabled = 1 ORDER BY icao ASC", conn)
            End If
            result = cmd.ExecuteReader()
            Do While result.Read()
                cboAircraft.Items.Add(result("name").ToString & " " & result("registration").ToString)
            Loop
            result.Close()
            cmdSubmit.Enabled = True
        Catch ex As Exception
            conn.Close()
            conn.Open()
            cboFlightBid.SelectedIndex = -1
        End Try
        
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles cmdBookForm.Click
        frmFlightBook.Show()
        Me.Hide()
    End Sub

    Private Sub frmFlopsCenter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Application.CurrentCulture = New CultureInfo("EN-US")
    End Sub

    Private Sub cmdValidate_Click(sender As Object, e As EventArgs) Handles cmdValidate.Click
        If cboAircraftVFR.SelectedIndex = -1 Then
            lblValidateVFR.Text = "Please Select Aircraft"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf cboDepVFR.SelectedIndex = -1 Then
            lblValidateVFR.Text = "Please Select Departure Airport"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf cboArrVFR.SelectedIndex = -1 Then
            lblValidateVFR.Text = "Please Select Arrival Airport"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf cboDepVFR.SelectedIndex <> cboArrVFR.SelectedIndex And cboAltnVFR.SelectedIndex = -1 Then
            lblValidateVFR.Text = "Please Select Alternate Airport"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf cboDepVFR.SelectedIndex <> cboArrVFR.SelectedIndex And cboAltnVFR.SelectedIndex = cboArrVFR.SelectedIndex Then
            lblValidateVFR.Text = "Alternate and Arrival Airport Must be Different"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf txtAltitudeVFR.Text = "" Then
            lblValidateVFR.Text = "Cruising Altitude Must Be Filled"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf Integer.Parse(txtAltitudeVFR.Text) < 1000 Then
            lblValidateVFR.Text = "Cruising Altitude Should Above 1000 Feet"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf Integer.Parse(txtAltitudeVFR.Text) > 14000 Then
            lblValidateVFR.Text = "Cruising Altitude Should Be 14.000 Feet Or Below"
            lblValidateVFR.ForeColor = Color.Red
        ElseIf txtTimeVFR.Text = "" Or txtTimeVFR.Text = "hh.mm" Then
            lblValidateVFR.Text = "Estimated Flight Time Must be Filled"
            lblValidateVFR.ForeColor = Color.Red
        Else
            lblValidateVFR.Text = "Flightplan Validation Success"
            lblValidateVFR.ForeColor = Color.Green

            cboAircraftVFR.Enabled = False
            cboArrVFR.Enabled = False
            cboDepVFR.Enabled = False
            cboAltnVFR.Enabled = False
            txtTimeVFR.Enabled = False
            txtAltitudeVFR.Enabled = False

            cmdValidate.Enabled = False
            cmdSubmitVFR.Enabled = True
        End If
    End Sub

    Private Sub txtTimeVFR_Enter(sender As Object, e As EventArgs) Handles txtTimeVFR.Enter
        txtTimeVFR.Clear()
    End Sub

    Private Sub cmdSubmitVFR_Click(sender As Object, e As EventArgs) Handles cmdSubmitVFR.Click
        frmFlightLog.routeid = "x"
        frmFlightLog.bidid = "x"
        '==================== Transfer Departure Airport ================
        Try
            Dim depVFR As String = cboDepVFR.SelectedItem.ToString
            depVFR = depVFR.Substring(0, 4)
            cmd = New MySqlCommand("SELECT * FROM phpvms_airports WHERE icao = '" & depVFR & "'", conn)
            result = cmd.ExecuteReader()
            result.Read()
            frmFlightLog.dep = depVFR
            frmFlightLog.depfull = result("name").ToString
            result.Close()

            '==================== Transfer Arrival Airport ================
            Dim arrVFR As String = cboArrVFR.SelectedItem.ToString
            arrVFR = depVFR.Substring(0, 4)
            cmd = New MySqlCommand("SELECT * FROM phpvms_airports WHERE icao = '" & arrVFR & "'", conn)
            result = cmd.ExecuteReader()
            result.Read()
            frmFlightLog.arr = arrVFR
            frmFlightLog.arrfull = result("name").ToString
            result.Close()

            '====================== Transfer Aircraft ======================
            Dim accombo As String = cboAircraftVFR.SelectedItem.ToString
            Dim acreg As String = accombo.Substring(accombo.Length - 6, 6)
            cmd = New MySqlCommand("SELECT * FROM phpvms_aircraft WHERE registration = '" & acreg & "'", conn)
            result = cmd.ExecuteReader()
            result.Read()
            frmFlightLog.acreg = result("registration").ToString
            frmFlightLog.actype = result("fullname").ToString
            frmFlightLog.ac_id = result("id").ToString
            result.Close()

            '==================== Transfer Other Value ===================
            frmFlightLog.fltno = txtCallsignVFR.Text
            frmFlightLog.route = "VFR"
            frmFlightLog.cruising = txtAltitudeVFR.Text
            frmFlightLog.opr = "Operated by NAM Flying School"

            '==================== Show Close Form ================
            frmFlightLog.Show()
            Me.Hide()
        Catch ex As Exception
            conn.Close()
            conn.Open()
            MessageBox.Show("Connection To Server Failed. Please Try Again.")
        End Try
    End Sub

    Private Sub cboAircraftVFR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAircraftVFR.SelectedIndexChanged
        Dim acft As String = cboAircraftVFR.SelectedItem.ToString
        txtCallsignVFR.Text = acft.Substring(acft.Length - 6, 2) & acft.Substring(acft.Length - 3, 3)
    End Sub

    Private Sub frmFlopsCenter_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Application.ExitThread()
    End Sub

    Private Sub frmFlopsCenter_VisibleChanged(sender As Object, e As EventArgs) Handles MyBase.VisibleChanged
        Try
            cboFlightBid.Items.Clear()
            cboAircraft.Items.Clear()
            cboAircraftVFR.Items.Clear()
            cboDepVFR.Items.Clear()
            cboArrVFR.Items.Clear()

            Dim serv As New Encryption
            conn = New MySqlConnection()
            conn.ConnectionString = serv.Decrypt(cfg("server"))
            conn.Open()
            '==================== Pilot Information ==================
            cmd = New MySqlCommand("SELECT * FROM phpvms_pilots WHERE pilotid = '" & frmLogin.lblPilotID.Text & "'", conn)
            result = cmd.ExecuteReader()

            Do While result.Read()
                lblName.Text = result("firstname").ToString & " " & result("lastname").ToString
                lblRank.Text = result("rank").ToString
                lblHours.Text = result("totalhours").ToString & " hours"
            Loop
            result.Close()

            '====================== Airport VFR ======================
            cmd = New MySqlCommand("SELECT * FROM phpvms_airports WHERE icao LIKE 'WI%' OR icao LIKE 'WA%' OR icao LIKE 'W4%' OR icao LIKE 'W3%' ORDER BY icao ASC", conn)
            result = cmd.ExecuteReader()

            Do While result.Read()
                cboDepVFR.Items.Add(result("icao").ToString & " - " & result("name").ToString)
                cboArrVFR.Items.Add(result("icao").ToString & " - " & result("name").ToString)
                cboAltnVFR.Items.Add(result("icao").ToString & " - " & result("name").ToString)
            Loop
            result.Close()

            '========================= Aircraft VFR ==================
            cmd = New MySqlCommand("SELECT * FROM phpvms_aircraft WHERE icao = 'P28R' AND enabled = 1 ORDER BY registration ASC", conn)
            result = cmd.ExecuteReader()

            Do While result.Read()
                cboAircraftVFR.Items.Add(result("fullname").ToString & " " & result("registration").ToString)
            Loop
            result.Close()

            '==================== Booked Flight ==================
            cmd = New MySqlCommand("SELECT a.* FROM phpvms_schedules a INNER JOIN phpvms_bids b ON a.bidid = b.bidid WHERE b.pilotid = '" & frmLogin.lblPilotID.Text & "'", conn)
            result = cmd.ExecuteReader()

            If result.HasRows Then
                Do While result.Read()
                    cboFlightBid.Items.Add(result("bidid").ToString & ": " & result("code").ToString & result("flightnum").ToString & " " & result("depicao").ToString & "-" & result("arricao").ToString)
                Loop
            End If
            result.Close()

        Catch ex As Exception
            MessageBox.Show("Connection Failed. Retrying...")
            Me.Refresh()
        End Try
    End Sub

    Private Sub frmFlopsCenter_Shown(sender As Object, e As EventArgs) Handles Me.Shown

    End Sub
End Class