﻿Imports MySql.Data.MySqlClient
Imports System.Configuration
Imports System.Globalization
Imports System.Threading

Public Class frmFlightBook

    Dim conn As MySqlConnection
    Dim querystring As String
    Dim result As MySqlDataReader
    Dim dt As DataTable
    Dim bindingSource1 As New BindingSource()
    Dim cmd As New MySqlCommand
    Dim cfg = ConfigurationManager.AppSettings

    Public Sub New()
        Dim myCi = New CultureInfo("en-US", False)
        myCi.NumberFormat.CurrencyDecimalSeparator = "."
        myCi.NumberFormat.CurrencyGroupSeparator = ","

        Thread.CurrentThread.CurrentCulture = myCi
        Thread.CurrentThread.CurrentUICulture = myCi

        InitializeComponent()
    End Sub

    Private Sub Form1_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Try
            Dim serv As New Encryption
            conn = New MySqlConnection()
            conn.ConnectionString = serv.Decrypt(cfg("server"))
            conn.Open()
            cmd = New MySqlCommand("SELECT id, flightnum, depicao, arricao, route, distance, flighttime, code FROM phpvms_schedules WHERE enabled = 1 AND bidid = 0", conn)
            Dim da = New MySqlDataAdapter(cmd)
            dt = New DataTable()
            da.Fill(dt)
            bindingSource1.DataSource = dt
            With DataGridViewX1
                .DataSource = bindingSource1
                .AutoGenerateColumns = True
            End With

        Catch ex As Exception
            MessageBox.Show("Error. Could Not Connect Server.")
        End Try
        DataGridViewX1.ClearSelection()
        TextBoxX1.Focus()
    End Sub

    Private Sub DataGridViewX1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewX1.CellClick
        Dim id As Object = DataGridViewX1.Rows(e.RowIndex).Cells(0).Value
        Dim flightnum As Object = DataGridViewX1.Rows(e.RowIndex).Cells(1).Value
        Dim depicao As Object = DataGridViewX1.Rows(e.RowIndex).Cells(2).Value
        Dim arricao As Object = DataGridViewX1.Rows(e.RowIndex).Cells(3).Value
        Dim distance As Object = DataGridViewX1.Rows(e.RowIndex).Cells(5).Value
        Dim duration As Object = DataGridViewX1.Rows(e.RowIndex).Cells(6).Value
        Dim route As Object = DataGridViewX1.Rows(e.RowIndex).Cells(4).Value
        Dim code As Object = DataGridViewX1.Rows(e.RowIndex).Cells(7).Value

        lblFltNum.Text = CType(code, String) & CType(flightnum, String)
        lblDep.Text = CType(depicao, String)
        lblArr.Text = CType(arricao, String)
        lblDist.Text = CType(distance, String) & " nm"
        lblTime.Text = CType(duration, String) & " hours"
        lblRoute.Text = CType(route, String)
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        If dt IsNot Nothing Then
            If TextBoxX1.Text <> "" And TextBoxX2.Text = "" Then
                bindingSource1.Filter = "depicao = '" & TextBoxX1.Text & "'"
            ElseIf TextBoxX2.Text <> "" And TextBoxX1.Text = "" Then
                bindingSource1.Filter = "arricao = '" & TextBoxX2.Text & "'"
            ElseIf TextBoxX2.Text <> "" And TextBoxX1.Text <> "" Then
                bindingSource1.Filter = "depicao = '" & TextBoxX1.Text & "' AND arricao = '" & TextBoxX2.Text & "'"
            End If
        End If
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles ButtonX2.Click
        bindingSource1.RemoveFilter()
        With TextBoxX1
            .Clear()
            .Focus()
        End With
        TextBoxX2.Clear()
    End Sub

    Private Sub cmdBook_Click(sender As Object, e As EventArgs) Handles cmdBook.Click
        Dim routeid As Object = DataGridViewX1.Rows(DataGridViewX1.CurrentRow.Index).Cells(0).Value

        Try
            Dim curdate As String = DateTime.Now.ToString("yyyy-MM-dd")
            cmd = New MySqlCommand("INSERT INTO phpvms_bids (pilotid, routeid, dateadded) VALUES ('" & frmLogin.lblPilotID.Text & "', '" & routeid & "', '" & curdate & "')", conn)
            cmd.ExecuteScalar()
            cmd = New MySqlCommand("SELECT * FROM phpvms_bids WHERE routeid = '" & routeid & "'", conn)
            result = cmd.ExecuteReader()
            result.Read()
            Dim bidid As String = result("bidid").ToString
            result.Close()
            cmd = New MySqlCommand("UPDATE phpvms_schedules SET bidid = '" & bidid & "' WHERE id = '" & routeid & "'", conn)
            cmd.ExecuteScalar()
            frmFlopsCenter.Show()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub frmFlightBook_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Application.CurrentCulture = New CultureInfo("EN-US")
    End Sub

    Private Sub frmFlightBook_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Me.Hide()
        frmFlopsCenter.Show()
    End Sub
End Class
