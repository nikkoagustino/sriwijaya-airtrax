﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFlightLog
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFlightLog))
        Me.MetroStatusBar1 = New DevComponents.DotNetBar.Metro.MetroStatusBar()
        Me.lblStatus = New DevComponents.DotNetBar.LabelItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblFlightNum = New DevComponents.DotNetBar.LabelX()
        Me.lblRoute = New DevComponents.DotNetBar.LabelX()
        Me.lblCruising = New DevComponents.DotNetBar.LabelX()
        Me.lblAircraft = New DevComponents.DotNetBar.LabelX()
        Me.lblArrival = New DevComponents.DotNetBar.LabelX()
        Me.lblDeparture = New DevComponents.DotNetBar.LabelX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.txtLog = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtComment = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.btnStart = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblFlighttime = New DevComponents.DotNetBar.LabelX()
        Me.lblAltitude = New DevComponents.DotNetBar.LabelX()
        Me.lblGroundspeed = New DevComponents.DotNetBar.LabelX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.lblPhase = New DevComponents.DotNetBar.LabelX()
        Me.lblLongitude = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.lblLatitude = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.btnStop = New DevComponents.DotNetBar.ButtonX()
        Me.btnSubmit = New DevComponents.DotNetBar.ButtonX()
        Me.tmrLog = New System.Windows.Forms.Timer(Me.components)
        Me.tmrLanding = New System.Windows.Forms.Timer(Me.components)
        Me.tmrAttitude = New System.Windows.Forms.Timer(Me.components)
        Me.tmrACARS = New System.Windows.Forms.Timer(Me.components)
        Me.txtCoordLog = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroStatusBar1
        '
        '
        '
        '
        Me.MetroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MetroStatusBar1.ContainerControlProcessDialogKey = True
        Me.MetroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MetroStatusBar1.Font = New System.Drawing.Font("Segoe UI", 10.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroStatusBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.lblStatus})
        Me.MetroStatusBar1.Location = New System.Drawing.Point(0, 452)
        Me.MetroStatusBar1.Name = "MetroStatusBar1"
        Me.MetroStatusBar1.Size = New System.Drawing.Size(603, 22)
        Me.MetroStatusBar1.TabIndex = 0
        Me.MetroStatusBar1.Text = "MetroStatusBar1"
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Text = "Flight Not Started"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFlightNum)
        Me.GroupBox1.Controls.Add(Me.lblRoute)
        Me.GroupBox1.Controls.Add(Me.lblCruising)
        Me.GroupBox1.Controls.Add(Me.lblAircraft)
        Me.GroupBox1.Controls.Add(Me.lblArrival)
        Me.GroupBox1.Controls.Add(Me.lblDeparture)
        Me.GroupBox1.Controls.Add(Me.LabelX11)
        Me.GroupBox1.Controls.Add(Me.LabelX10)
        Me.GroupBox1.Controls.Add(Me.LabelX9)
        Me.GroupBox1.Controls.Add(Me.LabelX8)
        Me.GroupBox1.Controls.Add(Me.LabelX7)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(314, 266)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Flight Detail"
        '
        'lblFlightNum
        '
        '
        '
        '
        Me.lblFlightNum.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblFlightNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFlightNum.Location = New System.Drawing.Point(174, 10)
        Me.lblFlightNum.Name = "lblFlightNum"
        Me.lblFlightNum.Size = New System.Drawing.Size(122, 23)
        Me.lblFlightNum.TabIndex = 10
        Me.lblFlightNum.TextAlignment = System.Drawing.StringAlignment.Far
        '
        'lblRoute
        '
        Me.lblRoute.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblRoute.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblRoute.Location = New System.Drawing.Point(25, 214)
        Me.lblRoute.Name = "lblRoute"
        Me.lblRoute.Size = New System.Drawing.Size(271, 38)
        Me.lblRoute.TabIndex = 9
        Me.lblRoute.TextLineAlignment = System.Drawing.StringAlignment.Near
        Me.lblRoute.WordWrap = True
        '
        'lblCruising
        '
        '
        '
        '
        Me.lblCruising.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblCruising.Location = New System.Drawing.Point(25, 172)
        Me.lblCruising.Name = "lblCruising"
        Me.lblCruising.Size = New System.Drawing.Size(283, 23)
        Me.lblCruising.TabIndex = 8
        '
        'lblAircraft
        '
        Me.lblAircraft.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblAircraft.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblAircraft.Location = New System.Drawing.Point(25, 133)
        Me.lblAircraft.Name = "lblAircraft"
        Me.lblAircraft.Size = New System.Drawing.Size(283, 23)
        Me.lblAircraft.TabIndex = 7
        '
        'lblArrival
        '
        '
        '
        '
        Me.lblArrival.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblArrival.Location = New System.Drawing.Point(25, 91)
        Me.lblArrival.Name = "lblArrival"
        Me.lblArrival.Size = New System.Drawing.Size(283, 23)
        Me.lblArrival.TabIndex = 6
        '
        'lblDeparture
        '
        Me.lblDeparture.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblDeparture.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblDeparture.Location = New System.Drawing.Point(25, 49)
        Me.lblDeparture.Name = "lblDeparture"
        Me.lblDeparture.Size = New System.Drawing.Size(283, 23)
        Me.lblDeparture.TabIndex = 5
        '
        'LabelX11
        '
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.Location = New System.Drawing.Point(12, 154)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(135, 23)
        Me.LabelX11.TabIndex = 4
        Me.LabelX11.Text = "Cruising Altitude"
        '
        'LabelX10
        '
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Location = New System.Drawing.Point(12, 193)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(135, 23)
        Me.LabelX10.TabIndex = 3
        Me.LabelX10.Text = "Flightplan Route"
        '
        'LabelX9
        '
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Location = New System.Drawing.Point(12, 115)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(75, 23)
        Me.LabelX9.TabIndex = 2
        Me.LabelX9.Text = "Aircraft In Use"
        '
        'LabelX8
        '
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Location = New System.Drawing.Point(12, 74)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(75, 23)
        Me.LabelX8.TabIndex = 1
        Me.LabelX8.Text = "Arrival Airport"
        '
        'LabelX7
        '
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Location = New System.Drawing.Point(12, 32)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(156, 23)
        Me.LabelX7.TabIndex = 0
        Me.LabelX7.Text = "Departure Airport"
        '
        'txtLog
        '
        Me.txtLog.AcceptsReturn = True
        Me.txtLog.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtLog.Border.Class = "TextBoxBorder"
        Me.txtLog.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLog.ForeColor = System.Drawing.Color.Black
        Me.txtLog.Location = New System.Drawing.Point(342, 26)
        Me.txtLog.Multiline = True
        Me.txtLog.Name = "txtLog"
        Me.txtLog.ReadOnly = True
        Me.txtLog.Size = New System.Drawing.Size(249, 310)
        Me.txtLog.TabIndex = 2
        '
        'txtComment
        '
        Me.txtComment.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtComment.Border.Class = "TextBoxBorder"
        Me.txtComment.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtComment.ForeColor = System.Drawing.Color.Black
        Me.txtComment.Location = New System.Drawing.Point(342, 364)
        Me.txtComment.Multiline = True
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(248, 41)
        Me.txtComment.TabIndex = 3
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(342, 342)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(148, 23)
        Me.LabelX1.TabIndex = 4
        Me.LabelX1.Text = "PIREP Comments"
        '
        'btnStart
        '
        Me.btnStart.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnStart.Location = New System.Drawing.Point(342, 411)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(69, 23)
        Me.btnStart.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnStart.TabIndex = 5
        Me.btnStart.Text = "Start Flight"
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(342, 4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 6
        Me.LabelX2.Text = "Flight Log"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblFlighttime)
        Me.GroupBox2.Controls.Add(Me.lblAltitude)
        Me.GroupBox2.Controls.Add(Me.lblGroundspeed)
        Me.GroupBox2.Controls.Add(Me.LabelX22)
        Me.GroupBox2.Controls.Add(Me.LabelX21)
        Me.GroupBox2.Controls.Add(Me.LabelX20)
        Me.GroupBox2.Controls.Add(Me.lblPhase)
        Me.GroupBox2.Controls.Add(Me.lblLongitude)
        Me.GroupBox2.Controls.Add(Me.LabelX6)
        Me.GroupBox2.Controls.Add(Me.LabelX5)
        Me.GroupBox2.Controls.Add(Me.lblLatitude)
        Me.GroupBox2.Controls.Add(Me.LabelX3)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 284)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(314, 150)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Flight Progress"
        '
        'lblFlighttime
        '
        '
        '
        '
        Me.lblFlighttime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblFlighttime.Location = New System.Drawing.Point(177, 121)
        Me.lblFlighttime.Name = "lblFlighttime"
        Me.lblFlighttime.Size = New System.Drawing.Size(131, 23)
        Me.lblFlighttime.TabIndex = 11
        '
        'lblAltitude
        '
        '
        '
        '
        Me.lblAltitude.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblAltitude.Location = New System.Drawing.Point(177, 77)
        Me.lblAltitude.Name = "lblAltitude"
        Me.lblAltitude.Size = New System.Drawing.Size(131, 23)
        Me.lblAltitude.TabIndex = 10
        '
        'lblGroundspeed
        '
        '
        '
        '
        Me.lblGroundspeed.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblGroundspeed.Location = New System.Drawing.Point(177, 36)
        Me.lblGroundspeed.Name = "lblGroundspeed"
        Me.lblGroundspeed.Size = New System.Drawing.Size(119, 23)
        Me.lblGroundspeed.TabIndex = 9
        '
        'LabelX22
        '
        '
        '
        '
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Location = New System.Drawing.Point(163, 103)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(110, 23)
        Me.LabelX22.TabIndex = 8
        Me.LabelX22.Text = "Actual Flight Time"
        '
        'LabelX21
        '
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.Location = New System.Drawing.Point(163, 59)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(75, 23)
        Me.LabelX21.TabIndex = 7
        Me.LabelX21.Text = "Altitude"
        '
        'LabelX20
        '
        '
        '
        '
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Location = New System.Drawing.Point(163, 19)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(75, 23)
        Me.LabelX20.TabIndex = 6
        Me.LabelX20.Text = "Ground Speed"
        '
        'lblPhase
        '
        '
        '
        '
        Me.lblPhase.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblPhase.Location = New System.Drawing.Point(25, 121)
        Me.lblPhase.Name = "lblPhase"
        Me.lblPhase.Size = New System.Drawing.Size(146, 23)
        Me.lblPhase.TabIndex = 5
        '
        'lblLongitude
        '
        '
        '
        '
        Me.lblLongitude.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblLongitude.Location = New System.Drawing.Point(25, 77)
        Me.lblLongitude.Name = "lblLongitude"
        Me.lblLongitude.Size = New System.Drawing.Size(136, 23)
        Me.lblLongitude.TabIndex = 4
        '
        'LabelX6
        '
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(15, 103)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(75, 23)
        Me.LabelX6.TabIndex = 3
        Me.LabelX6.Text = "Flight Phase"
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(12, 59)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(149, 23)
        Me.LabelX5.TabIndex = 2
        Me.LabelX5.Text = "Current Longitude"
        '
        'lblLatitude
        '
        '
        '
        '
        Me.lblLatitude.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblLatitude.Location = New System.Drawing.Point(25, 36)
        Me.lblLatitude.Name = "lblLatitude"
        Me.lblLatitude.Size = New System.Drawing.Size(136, 23)
        Me.lblLatitude.TabIndex = 1
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(12, 19)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(136, 23)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Current Latitude"
        '
        'btnStop
        '
        Me.btnStop.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnStop.Enabled = False
        Me.btnStop.Location = New System.Drawing.Point(417, 411)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(69, 23)
        Me.btnStop.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnStop.TabIndex = 8
        Me.btnStop.Text = "Stop Flight"
        '
        'btnSubmit
        '
        Me.btnSubmit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSubmit.Enabled = False
        Me.btnSubmit.Location = New System.Drawing.Point(492, 411)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(98, 23)
        Me.btnSubmit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnSubmit.TabIndex = 9
        Me.btnSubmit.Text = "Submit PIREP >>"
        '
        'tmrLog
        '
        Me.tmrLog.Interval = 1000
        '
        'tmrLanding
        '
        Me.tmrLanding.Interval = 1
        '
        'tmrAttitude
        '
        Me.tmrAttitude.Interval = 5000
        '
        'tmrACARS
        '
        Me.tmrACARS.Interval = 5000
        '
        'txtCoordLog
        '
        Me.txtCoordLog.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCoordLog.Border.Class = "TextBoxBorder"
        Me.txtCoordLog.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCoordLog.ForeColor = System.Drawing.Color.Black
        Me.txtCoordLog.Location = New System.Drawing.Point(342, 284)
        Me.txtCoordLog.Multiline = True
        Me.txtCoordLog.Name = "txtCoordLog"
        Me.txtCoordLog.Size = New System.Drawing.Size(249, 54)
        Me.txtCoordLog.TabIndex = 10
        Me.txtCoordLog.Visible = False
        '
        'BackgroundWorker1
        '
        '
        'frmFlightLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(603, 474)
        Me.Controls.Add(Me.txtCoordLog)
        Me.Controls.Add(Me.btnSubmit)
        Me.Controls.Add(Me.btnStop)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtLog)
        Me.Controls.Add(Me.LabelX2)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MetroStatusBar1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmFlightLog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Flight Logging - Sriwijaya AirTrax"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MetroStatusBar1 As DevComponents.DotNetBar.Metro.MetroStatusBar
    Friend WithEvents lblStatus As DevComponents.DotNetBar.LabelItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtLog As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lblDeparture As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtComment As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnStart As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblLatitude As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnStop As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnSubmit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lblFlightNum As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblRoute As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblCruising As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblAircraft As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblArrival As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblFlighttime As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblAltitude As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblGroundspeed As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblPhase As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblLongitude As DevComponents.DotNetBar.LabelX
    Friend WithEvents tmrLog As System.Windows.Forms.Timer
    Friend WithEvents tmrLanding As System.Windows.Forms.Timer
    Friend WithEvents tmrAttitude As System.Windows.Forms.Timer
    Friend WithEvents tmrACARS As System.Windows.Forms.Timer
    Friend WithEvents txtCoordLog As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
