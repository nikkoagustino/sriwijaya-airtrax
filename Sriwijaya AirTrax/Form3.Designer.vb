﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFlopsCenter
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFlopsCenter))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblName = New DevComponents.DotNetBar.LabelX()
        Me.lblHours = New DevComponents.DotNetBar.LabelX()
        Me.lblRank = New DevComponents.DotNetBar.LabelX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboAircraft = New System.Windows.Forms.ComboBox()
        Me.cboFlightBid = New System.Windows.Forms.ComboBox()
        Me.txtCrz = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtEET = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtAltn = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtArr = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDep = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtFltNo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtRoute = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSubmit = New DevComponents.DotNetBar.ButtonX()
        Me.cmdBookForm = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblValidateVFR = New DevComponents.DotNetBar.LabelX()
        Me.cboAltnVFR = New System.Windows.Forms.ComboBox()
        Me.cboArrVFR = New System.Windows.Forms.ComboBox()
        Me.cboDepVFR = New System.Windows.Forms.ComboBox()
        Me.cboAircraftVFR = New System.Windows.Forms.ComboBox()
        Me.txtTimeVFR = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtAltitudeVFR = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCallsignVFR = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSubmitVFR = New DevComponents.DotNetBar.ButtonX()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.MetroStatusBar1 = New DevComponents.DotNetBar.Metro.MetroStatusBar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.lblName)
        Me.GroupBox1.Controls.Add(Me.lblHours)
        Me.GroupBox1.Controls.Add(Me.lblRank)
        Me.GroupBox1.Controls.Add(Me.LabelX12)
        Me.GroupBox1.Controls.Add(Me.LabelX11)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(313, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(278, 84)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pilot Information"
        '
        'lblName
        '
        Me.lblName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lblName.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblName.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.ForeColor = System.Drawing.Color.Black
        Me.lblName.Location = New System.Drawing.Point(6, 13)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(266, 23)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Pilot Name"
        Me.lblName.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'lblHours
        '
        Me.lblHours.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lblHours.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblHours.ForeColor = System.Drawing.Color.Black
        Me.lblHours.Location = New System.Drawing.Point(96, 55)
        Me.lblHours.Name = "lblHours"
        Me.lblHours.Size = New System.Drawing.Size(176, 23)
        Me.lblHours.TabIndex = 4
        Me.lblHours.Text = "Pilot Hours"
        '
        'lblRank
        '
        Me.lblRank.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lblRank.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblRank.ForeColor = System.Drawing.Color.Black
        Me.lblRank.Location = New System.Drawing.Point(96, 35)
        Me.lblRank.Name = "lblRank"
        Me.lblRank.Size = New System.Drawing.Size(176, 23)
        Me.lblRank.TabIndex = 3
        Me.lblRank.Text = "Pilot Rank"
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(15, 55)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(75, 23)
        Me.LabelX12.TabIndex = 2
        Me.LabelX12.Text = "Hours Flown"
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(15, 35)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(75, 23)
        Me.LabelX11.TabIndex = 1
        Me.LabelX11.Text = "Current Rank"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.White
        Me.GroupBox2.Controls.Add(Me.cboAircraft)
        Me.GroupBox2.Controls.Add(Me.cboFlightBid)
        Me.GroupBox2.Controls.Add(Me.txtCrz)
        Me.GroupBox2.Controls.Add(Me.txtEET)
        Me.GroupBox2.Controls.Add(Me.txtAltn)
        Me.GroupBox2.Controls.Add(Me.txtArr)
        Me.GroupBox2.Controls.Add(Me.txtDep)
        Me.GroupBox2.Controls.Add(Me.txtFltNo)
        Me.GroupBox2.Controls.Add(Me.txtRoute)
        Me.GroupBox2.Controls.Add(Me.cmdSubmit)
        Me.GroupBox2.Controls.Add(Me.cmdBookForm)
        Me.GroupBox2.Controls.Add(Me.LabelX9)
        Me.GroupBox2.Controls.Add(Me.LabelX8)
        Me.GroupBox2.Controls.Add(Me.LabelX7)
        Me.GroupBox2.Controls.Add(Me.LabelX6)
        Me.GroupBox2.Controls.Add(Me.LabelX5)
        Me.GroupBox2.Controls.Add(Me.LabelX4)
        Me.GroupBox2.Controls.Add(Me.LabelX3)
        Me.GroupBox2.Controls.Add(Me.LabelX2)
        Me.GroupBox2.Controls.Add(Me.LabelX1)
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(280, 428)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Fly With Sriwijaya Air Group"
        '
        'cboAircraft
        '
        Me.cboAircraft.BackColor = System.Drawing.Color.White
        Me.cboAircraft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAircraft.ForeColor = System.Drawing.Color.Black
        Me.cboAircraft.FormattingEnabled = True
        Me.cboAircraft.IntegralHeight = False
        Me.cboAircraft.Location = New System.Drawing.Point(148, 228)
        Me.cboAircraft.MaxDropDownItems = 7
        Me.cboAircraft.Name = "cboAircraft"
        Me.cboAircraft.Size = New System.Drawing.Size(112, 21)
        Me.cboAircraft.TabIndex = 7
        '
        'cboFlightBid
        '
        Me.cboFlightBid.BackColor = System.Drawing.Color.White
        Me.cboFlightBid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFlightBid.ForeColor = System.Drawing.Color.Black
        Me.cboFlightBid.FormattingEnabled = True
        Me.cboFlightBid.IntegralHeight = False
        Me.cboFlightBid.Location = New System.Drawing.Point(15, 27)
        Me.cboFlightBid.MaxDropDownItems = 7
        Me.cboFlightBid.Name = "cboFlightBid"
        Me.cboFlightBid.Size = New System.Drawing.Size(245, 21)
        Me.cboFlightBid.TabIndex = 0
        '
        'txtCrz
        '
        Me.txtCrz.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCrz.Border.Class = "TextBoxBorder"
        Me.txtCrz.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCrz.ForeColor = System.Drawing.Color.Black
        Me.txtCrz.Location = New System.Drawing.Point(148, 287)
        Me.txtCrz.Name = "txtCrz"
        Me.txtCrz.Size = New System.Drawing.Size(112, 20)
        Me.txtCrz.TabIndex = 9
        '
        'txtEET
        '
        Me.txtEET.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtEET.Border.Class = "TextBoxBorder"
        Me.txtEET.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEET.Enabled = False
        Me.txtEET.ForeColor = System.Drawing.Color.Black
        Me.txtEET.Location = New System.Drawing.Point(148, 258)
        Me.txtEET.Name = "txtEET"
        Me.txtEET.Size = New System.Drawing.Size(112, 20)
        Me.txtEET.TabIndex = 8
        '
        'txtAltn
        '
        Me.txtAltn.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtAltn.Border.Class = "TextBoxBorder"
        Me.txtAltn.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAltn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAltn.ForeColor = System.Drawing.Color.Black
        Me.txtAltn.Location = New System.Drawing.Point(148, 200)
        Me.txtAltn.Name = "txtAltn"
        Me.txtAltn.Size = New System.Drawing.Size(112, 20)
        Me.txtAltn.TabIndex = 6
        '
        'txtArr
        '
        Me.txtArr.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtArr.Border.Class = "TextBoxBorder"
        Me.txtArr.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtArr.Enabled = False
        Me.txtArr.ForeColor = System.Drawing.Color.Black
        Me.txtArr.Location = New System.Drawing.Point(148, 171)
        Me.txtArr.Name = "txtArr"
        Me.txtArr.Size = New System.Drawing.Size(112, 20)
        Me.txtArr.TabIndex = 5
        '
        'txtDep
        '
        Me.txtDep.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDep.Border.Class = "TextBoxBorder"
        Me.txtDep.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDep.Enabled = False
        Me.txtDep.ForeColor = System.Drawing.Color.Black
        Me.txtDep.Location = New System.Drawing.Point(148, 142)
        Me.txtDep.Name = "txtDep"
        Me.txtDep.Size = New System.Drawing.Size(112, 20)
        Me.txtDep.TabIndex = 3
        '
        'txtFltNo
        '
        Me.txtFltNo.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFltNo.Border.Class = "TextBoxBorder"
        Me.txtFltNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFltNo.Enabled = False
        Me.txtFltNo.ForeColor = System.Drawing.Color.Black
        Me.txtFltNo.Location = New System.Drawing.Point(148, 113)
        Me.txtFltNo.Name = "txtFltNo"
        Me.txtFltNo.ReadOnly = True
        Me.txtFltNo.Size = New System.Drawing.Size(112, 20)
        Me.txtFltNo.TabIndex = 2
        '
        'txtRoute
        '
        Me.txtRoute.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtRoute.Border.Class = "TextBoxBorder"
        Me.txtRoute.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtRoute.ForeColor = System.Drawing.Color.Black
        Me.txtRoute.Location = New System.Drawing.Point(15, 336)
        Me.txtRoute.Multiline = True
        Me.txtRoute.Name = "txtRoute"
        Me.txtRoute.Size = New System.Drawing.Size(245, 46)
        Me.txtRoute.TabIndex = 10
        '
        'cmdSubmit
        '
        Me.cmdSubmit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSubmit.Enabled = False
        Me.cmdSubmit.Location = New System.Drawing.Point(131, 388)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(129, 23)
        Me.cmdSubmit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdSubmit.TabIndex = 11
        Me.cmdSubmit.Text = "Submit Flightplan >>"
        '
        'cmdBookForm
        '
        Me.cmdBookForm.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBookForm.Location = New System.Drawing.Point(15, 73)
        Me.cmdBookForm.Name = "cmdBookForm"
        Me.cmdBookForm.Size = New System.Drawing.Size(118, 23)
        Me.cmdBookForm.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdBookForm.TabIndex = 1
        Me.cmdBookForm.Text = "Book Another Flight"
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.ForeColor = System.Drawing.Color.Black
        Me.LabelX9.Location = New System.Drawing.Point(15, 47)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(75, 23)
        Me.LabelX9.TabIndex = 10
        Me.LabelX9.Text = "- or -"
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(15, 255)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(140, 23)
        Me.LabelX8.TabIndex = 9
        Me.LabelX8.Text = "Estimated Enroute Time"
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(15, 110)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(75, 23)
        Me.LabelX7.TabIndex = 7
        Me.LabelX7.Text = "Flight Number"
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(15, 284)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(104, 23)
        Me.LabelX6.TabIndex = 6
        Me.LabelX6.Text = "Cruising Altitude"
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(15, 313)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(162, 23)
        Me.LabelX5.TabIndex = 5
        Me.LabelX5.Text = "IFR Flightplan Route"
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(15, 226)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(75, 23)
        Me.LabelX4.TabIndex = 4
        Me.LabelX4.Text = "Aircraft In Use"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(15, 197)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(75, 23)
        Me.LabelX3.TabIndex = 3
        Me.LabelX3.Text = "Alternate ICAO"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(15, 168)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 2
        Me.LabelX2.Text = "Arrival ICAO"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(15, 139)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(106, 23)
        Me.LabelX1.TabIndex = 1
        Me.LabelX1.Text = "Departure ICAO"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.White
        Me.GroupBox3.Controls.Add(Me.lblValidateVFR)
        Me.GroupBox3.Controls.Add(Me.cboAltnVFR)
        Me.GroupBox3.Controls.Add(Me.cboArrVFR)
        Me.GroupBox3.Controls.Add(Me.cboDepVFR)
        Me.GroupBox3.Controls.Add(Me.cboAircraftVFR)
        Me.GroupBox3.Controls.Add(Me.txtTimeVFR)
        Me.GroupBox3.Controls.Add(Me.txtAltitudeVFR)
        Me.GroupBox3.Controls.Add(Me.txtCallsignVFR)
        Me.GroupBox3.Controls.Add(Me.cmdSubmitVFR)
        Me.GroupBox3.Controls.Add(Me.cmdValidate)
        Me.GroupBox3.Controls.Add(Me.LabelX21)
        Me.GroupBox3.Controls.Add(Me.LabelX20)
        Me.GroupBox3.Controls.Add(Me.LabelX19)
        Me.GroupBox3.Controls.Add(Me.LabelX18)
        Me.GroupBox3.Controls.Add(Me.LabelX17)
        Me.GroupBox3.Controls.Add(Me.LabelX16)
        Me.GroupBox3.Controls.Add(Me.LabelX15)
        Me.GroupBox3.ForeColor = System.Drawing.Color.Black
        Me.GroupBox3.Location = New System.Drawing.Point(313, 102)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(278, 338)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "or Assign NAM Flying School VFR Flight"
        '
        'lblValidateVFR
        '
        Me.lblValidateVFR.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lblValidateVFR.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblValidateVFR.ForeColor = System.Drawing.Color.DimGray
        Me.lblValidateVFR.Location = New System.Drawing.Point(15, 274)
        Me.lblValidateVFR.Name = "lblValidateVFR"
        Me.lblValidateVFR.Size = New System.Drawing.Size(244, 18)
        Me.lblValidateVFR.TabIndex = 20
        Me.lblValidateVFR.Text = "Click Validate Flightplan To Continue"
        Me.lblValidateVFR.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'cboAltnVFR
        '
        Me.cboAltnVFR.BackColor = System.Drawing.Color.White
        Me.cboAltnVFR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAltnVFR.ForeColor = System.Drawing.Color.Black
        Me.cboAltnVFR.FormattingEnabled = True
        Me.cboAltnVFR.IntegralHeight = False
        Me.cboAltnVFR.Location = New System.Drawing.Point(15, 162)
        Me.cboAltnVFR.MaxDropDownItems = 7
        Me.cboAltnVFR.Name = "cboAltnVFR"
        Me.cboAltnVFR.Size = New System.Drawing.Size(244, 21)
        Me.cboAltnVFR.TabIndex = 15
        '
        'cboArrVFR
        '
        Me.cboArrVFR.BackColor = System.Drawing.Color.White
        Me.cboArrVFR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArrVFR.ForeColor = System.Drawing.Color.Black
        Me.cboArrVFR.FormattingEnabled = True
        Me.cboArrVFR.IntegralHeight = False
        Me.cboArrVFR.Location = New System.Drawing.Point(15, 119)
        Me.cboArrVFR.MaxDropDownItems = 7
        Me.cboArrVFR.Name = "cboArrVFR"
        Me.cboArrVFR.Size = New System.Drawing.Size(244, 21)
        Me.cboArrVFR.TabIndex = 14
        '
        'cboDepVFR
        '
        Me.cboDepVFR.BackColor = System.Drawing.Color.White
        Me.cboDepVFR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepVFR.ForeColor = System.Drawing.Color.Black
        Me.cboDepVFR.FormattingEnabled = True
        Me.cboDepVFR.IntegralHeight = False
        Me.cboDepVFR.Location = New System.Drawing.Point(15, 75)
        Me.cboDepVFR.MaxDropDownItems = 7
        Me.cboDepVFR.Name = "cboDepVFR"
        Me.cboDepVFR.Size = New System.Drawing.Size(244, 21)
        Me.cboDepVFR.TabIndex = 13
        '
        'cboAircraftVFR
        '
        Me.cboAircraftVFR.BackColor = System.Drawing.Color.White
        Me.cboAircraftVFR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAircraftVFR.ForeColor = System.Drawing.Color.Black
        Me.cboAircraftVFR.FormattingEnabled = True
        Me.cboAircraftVFR.IntegralHeight = False
        Me.cboAircraftVFR.Location = New System.Drawing.Point(15, 31)
        Me.cboAircraftVFR.MaxDropDownItems = 7
        Me.cboAircraftVFR.Name = "cboAircraftVFR"
        Me.cboAircraftVFR.Size = New System.Drawing.Size(244, 21)
        Me.cboAircraftVFR.TabIndex = 12
        '
        'txtTimeVFR
        '
        Me.txtTimeVFR.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtTimeVFR.Border.Class = "TextBoxBorder"
        Me.txtTimeVFR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTimeVFR.ForeColor = System.Drawing.Color.Black
        Me.txtTimeVFR.Location = New System.Drawing.Point(159, 248)
        Me.txtTimeVFR.Name = "txtTimeVFR"
        Me.txtTimeVFR.Size = New System.Drawing.Size(100, 20)
        Me.txtTimeVFR.TabIndex = 18
        Me.txtTimeVFR.Text = "hh.mm"
        '
        'txtAltitudeVFR
        '
        Me.txtAltitudeVFR.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtAltitudeVFR.Border.Class = "TextBoxBorder"
        Me.txtAltitudeVFR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAltitudeVFR.ForeColor = System.Drawing.Color.Black
        Me.txtAltitudeVFR.Location = New System.Drawing.Point(159, 219)
        Me.txtAltitudeVFR.Name = "txtAltitudeVFR"
        Me.txtAltitudeVFR.Size = New System.Drawing.Size(100, 20)
        Me.txtAltitudeVFR.TabIndex = 17
        '
        'txtCallsignVFR
        '
        Me.txtCallsignVFR.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCallsignVFR.Border.Class = "TextBoxBorder"
        Me.txtCallsignVFR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCallsignVFR.Enabled = False
        Me.txtCallsignVFR.ForeColor = System.Drawing.Color.Black
        Me.txtCallsignVFR.Location = New System.Drawing.Point(159, 190)
        Me.txtCallsignVFR.Name = "txtCallsignVFR"
        Me.txtCallsignVFR.Size = New System.Drawing.Size(100, 20)
        Me.txtCallsignVFR.TabIndex = 16
        '
        'cmdSubmitVFR
        '
        Me.cmdSubmitVFR.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSubmitVFR.Enabled = False
        Me.cmdSubmitVFR.Location = New System.Drawing.Point(138, 298)
        Me.cmdSubmitVFR.Name = "cmdSubmitVFR"
        Me.cmdSubmitVFR.Size = New System.Drawing.Size(121, 23)
        Me.cmdSubmitVFR.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdSubmitVFR.TabIndex = 20
        Me.cmdSubmitVFR.Text = "Submit Flightplan >>"
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.Location = New System.Drawing.Point(15, 298)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(117, 23)
        Me.cmdValidate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdValidate.TabIndex = 19
        Me.cmdValidate.Text = "Validate Flightplan"
        '
        'LabelX21
        '
        Me.LabelX21.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.ForeColor = System.Drawing.Color.Black
        Me.LabelX21.Location = New System.Drawing.Point(15, 187)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(75, 23)
        Me.LabelX21.TabIndex = 10
        Me.LabelX21.Text = "Callsign"
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(15, 245)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(132, 23)
        Me.LabelX20.TabIndex = 9
        Me.LabelX20.Text = "Estimated Flight Time"
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(15, 216)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(102, 23)
        Me.LabelX19.TabIndex = 8
        Me.LabelX19.Text = "Cruising Altitude"
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(15, 141)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(227, 23)
        Me.LabelX18.TabIndex = 5
        Me.LabelX18.Text = "Alternate Airport (Cross Country VFR Only)"
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(15, 98)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(75, 23)
        Me.LabelX17.TabIndex = 4
        Me.LabelX17.Text = "Arrival Airport"
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(15, 54)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(152, 23)
        Me.LabelX16.TabIndex = 1
        Me.LabelX16.Text = "Departure Airport"
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(15, 12)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(75, 23)
        Me.LabelX15.TabIndex = 0
        Me.LabelX15.Text = "Select Aircraft"
        '
        'MetroStatusBar1
        '
        Me.MetroStatusBar1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.MetroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MetroStatusBar1.ContainerControlProcessDialogKey = True
        Me.MetroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MetroStatusBar1.Font = New System.Drawing.Font("Segoe UI", 10.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroStatusBar1.ForeColor = System.Drawing.Color.Black
        Me.MetroStatusBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1})
        Me.MetroStatusBar1.Location = New System.Drawing.Point(0, 452)
        Me.MetroStatusBar1.Name = "MetroStatusBar1"
        Me.MetroStatusBar1.Size = New System.Drawing.Size(603, 22)
        Me.MetroStatusBar1.TabIndex = 3
        Me.MetroStatusBar1.Text = "Ready"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "Ready"
        '
        'frmFlopsCenter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(603, 474)
        Me.Controls.Add(Me.MetroStatusBar1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmFlopsCenter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Flight Operation Center - Sriwijaya AirTrax"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents lblHours As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblRank As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblName As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCrz As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtEET As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtAltn As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtArr As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDep As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtFltNo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtRoute As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdSubmit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdBookForm As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTimeVFR As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtAltitudeVFR As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCallsignVFR As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdSubmitVFR As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents MetroStatusBar1 As DevComponents.DotNetBar.Metro.MetroStatusBar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cboFlightBid As System.Windows.Forms.ComboBox
    Friend WithEvents cboAircraft As System.Windows.Forms.ComboBox
    Friend WithEvents cboAircraftVFR As System.Windows.Forms.ComboBox
    Friend WithEvents cboAltnVFR As System.Windows.Forms.ComboBox
    Friend WithEvents cboArrVFR As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepVFR As System.Windows.Forms.ComboBox
    Friend WithEvents lblValidateVFR As DevComponents.DotNetBar.LabelX
End Class
