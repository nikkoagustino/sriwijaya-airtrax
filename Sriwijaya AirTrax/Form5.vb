﻿Imports MySql.Data.MySqlClient
Imports System.Configuration
Imports System.ComponentModel
Imports System.Threading
Imports System.Globalization

Public Class Form5
    Dim conn As MySqlConnection
    Dim querystring As String
    Dim result As MySqlDataReader
    Dim cmd As MySqlCommand
    Dim cfg = ConfigurationManager.AppSettings
    Public version As String = "3.0.4"

    Public Sub New()
        Dim myCi = New CultureInfo("en-US", False)
        myCi.NumberFormat.CurrencyDecimalSeparator = "."
        myCi.NumberFormat.NumberDecimalSeparator = "."
        myCi.NumberFormat.CurrencyGroupSeparator = ","
        myCi.NumberFormat.NumberGroupSeparator = ","
        myCi.NumberFormat.PercentDecimalSeparator = "."
        myCi.NumberFormat.PercentGroupSeparator = ","

        Thread.CurrentThread.CurrentCulture = myCi
        Thread.CurrentThread.CurrentUICulture = myCi

        InitializeComponent()
    End Sub

    Public Sub CheckForExistingInstance()
        If Process.GetProcessesByName _
          (Process.GetCurrentProcess.ProcessName).Length > 1 Then

            MessageBox.Show _
             ("Another Instance of This Process is Already Running.", _
                 "Please Close All Running AirTrax or Kill Process From Task Manager.", _
                  MessageBoxButtons.OK, _
                 MessageBoxIcon.Exclamation)
            Application.Exit()
        End If
    End Sub

    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles Me.Load
        CheckForExistingInstance()
    End Sub

    Private Sub Form5_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            Dim serv As New Encryption
            conn = New MySqlConnection()
            conn.ConnectionString = serv.Decrypt(cfg("server"))
            conn.Open()

            cmd = New MySqlCommand("SELECT * FROM airtrax_update WHERE version = '" & version & "'", conn)
            result = cmd.ExecuteReader()
            result.Read()
            If String.IsNullOrEmpty(result("available")) = False And result("available") = 1 Then
                Thread.Sleep(4000)
                frmLogin.Show()
                Me.Close()
            Else
                Timer1.Enabled = False
                MessageBox.Show("Newer AirTrax Version Available. Please Download Latest Version.")
                Dim web As String = "http://airtrax.sriwijayavirtual.net"
                Process.Start(web)
                Me.Close()
            End If
        Catch ex As Exception
            conn.Close()
            Timer1.Enabled = False
            MessageBox.Show("Connection Failed. Verify internet connected and click OK to restart AirTrax.")
            Application.Restart()
        End Try
    End Sub

End Class