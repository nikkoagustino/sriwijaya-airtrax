﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogin))
        Me.btnLogin = New DevComponents.DotNetBar.ButtonX()
        Me.btnClear = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtPilotID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkSave = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.lblPilotID = New DevComponents.DotNetBar.LabelX()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.SuspendLayout()
        '
        'btnLogin
        '
        Me.btnLogin.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnLogin.Location = New System.Drawing.Point(29, 150)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(114, 23)
        Me.btnLogin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnLogin.TabIndex = 3
        Me.btnLogin.Text = "Login >>"
        '
        'btnClear
        '
        Me.btnClear.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnClear.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground
        Me.btnClear.Location = New System.Drawing.Point(149, 150)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(97, 23)
        Me.btnClear.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnClear.TabIndex = 4
        Me.btnClear.Text = "Clear"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(29, 34)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 9
        Me.LabelX1.Text = "Pilot ID"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(29, 73)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 9
        Me.LabelX2.Text = "Password"
        '
        'txtPilotID
        '
        Me.txtPilotID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPilotID.Border.Class = "TextBoxBorder"
        Me.txtPilotID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPilotID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPilotID.FocusHighlightColor = System.Drawing.Color.DeepSkyBlue
        Me.txtPilotID.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPilotID.ForeColor = System.Drawing.Color.Black
        Me.txtPilotID.Location = New System.Drawing.Point(105, 34)
        Me.txtPilotID.Name = "txtPilotID"
        Me.txtPilotID.Size = New System.Drawing.Size(141, 23)
        Me.txtPilotID.TabIndex = 0
        '
        'txtPassword
        '
        Me.txtPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.FocusHighlightColor = System.Drawing.Color.DeepSkyBlue
        Me.txtPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.ForeColor = System.Drawing.Color.Black
        Me.txtPassword.Location = New System.Drawing.Point(104, 73)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(142, 23)
        Me.txtPassword.TabIndex = 1
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'chkSave
        '
        Me.chkSave.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.chkSave.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSave.ForeColor = System.Drawing.Color.Black
        Me.chkSave.Location = New System.Drawing.Point(105, 112)
        Me.chkSave.Name = "chkSave"
        Me.chkSave.Size = New System.Drawing.Size(136, 22)
        Me.chkSave.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkSave.TabIndex = 2
        Me.chkSave.Text = "Save Password"
        '
        'lblPilotID
        '
        '
        '
        '
        Me.lblPilotID.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblPilotID.Location = New System.Drawing.Point(29, 6)
        Me.lblPilotID.Name = "lblPilotID"
        Me.lblPilotID.Size = New System.Drawing.Size(75, 23)
        Me.lblPilotID.TabIndex = 10
        Me.lblPilotID.Visible = False
        '
        'frmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(286, 210)
        Me.Controls.Add(Me.lblPilotID)
        Me.Controls.Add(Me.chkSave)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtPilotID)
        Me.Controls.Add(Me.LabelX2)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnLogin)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login - Sriwijaya AirTrax"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnLogin As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnClear As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPilotID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkSave As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents lblPilotID As DevComponents.DotNetBar.LabelX
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
