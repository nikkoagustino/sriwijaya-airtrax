﻿Imports System.Configuration
Imports FSUIPC
Imports System.Math
Imports MySql.Data.MySqlClient
Imports System
Imports System.IO
Imports System.Text
Imports System.Globalization
Imports System.Threading

Public Class frmFlightLog
    Public dep As String
    Public depfull As String
    Public arr As String
    Public arrfull As String
    Public fltno As String
    Public route As String
    Public opr As String
    Public acreg As String
    Public actype As String
    Public ac_id As String
    Public cruising As String
    Public pilotid As String = frmLogin.lblPilotID.Text
    Dim pilotname As String = frmFlopsCenter.lblName.Text
    Public routeid As String
    Public bidid As String

    Dim appPath As String = Application.StartupPath()
    Dim cfg = ConfigurationManager.AppSettings
    Dim serv As New Encryption
    Dim conn As MySqlConnection = New MySqlConnection(serv.Decrypt(cfg("server")))
    Dim result As MySqlDataReader
    Dim cmd As MySqlCommand
    Dim bgworkstatus As Boolean
    Dim status As String
    Dim acarsavailable As Integer
    Dim deptime As String
    Dim arrtime As String

    '=========== U8 - 1
    Dim fsstall As Offset(Of Byte) = New FSUIPC.Offset(Of Byte)(&H36C)
    Dim fsover As Offset(Of Byte) = New FSUIPC.Offset(Of Byte)(&H36D)
    Dim hours As Offset(Of Byte) = New FSUIPC.Offset(Of Byte)(&H238)
    Dim mins As Offset(Of Byte) = New FSUIPC.Offset(Of Byte)(&H239)
    Dim navlight As Offset(Of Byte) = New FSUIPC.Offset(Of Byte)(&H280)
    Dim strobelight As Offset(Of Byte) = New FSUIPC.Offset(Of Byte)(&H281)
    Dim landinglight As Offset(Of Byte) = New FSUIPC.Offset(Of Byte)(&H28C)
    '=========== U16 - 2
    Dim eng1stat As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H894)
    Dim eng2stat As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H92C)
    Dim xpdr As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H354)
    Dim com1 As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H34E)
    Dim com2 As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H3118)
    Dim nav1 As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H350)
    Dim nav2 As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H352)
    Dim adf1 As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H34C)
    Dim adf2 As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H2D4)
    Dim adf1ext As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H356)
    Dim adf2ext As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H2D6)
    Dim airborne As Offset(Of UShort) = New FSUIPC.Offset(Of UShort)(&H366)
    '=========== S16 - 2
    Dim pause As Offset(Of Short) = New FSUIPC.Offset(Of Short)(&H262, True)
    Dim simrate As Offset(Of Short) = New FSUIPC.Offset(Of Short)(&HC1A, True)
    Dim slewMode As Offset(Of Short) = New FSUIPC.Offset(Of Short)(&H5DC)
    Dim parkbrake As Offset(Of Short) = New FSUIPC.Offset(Of Short)(&HBC8)
    Dim elevation As Offset(Of Short) = New FSUIPC.Offset(Of Short)(&HB4C)
    '=========== U32 - 4
    Dim heading As Offset(Of UInteger) = New FSUIPC.Offset(Of UInteger)(&H580)
    Dim pushback As Offset(Of UInteger) = New FSUIPC.Offset(Of UInteger)(&H31F0)
    '=========== S32 - 4
    Dim groundspeed As Offset(Of Integer) = New FSUIPC.Offset(Of Integer)(&H2B4)
    Dim flaps As Offset(Of Integer) = New FSUIPC.Offset(Of Integer)(&HBDC)
    Dim zfw As Offset(Of Integer) = New FSUIPC.Offset(Of Integer)(&H3BFC)
    Dim gear As Offset(Of Integer) = New FSUIPC.Offset(Of Integer)(&HBE8)
    Dim tdvs As Offset(Of Integer) = New FSUIPC.Offset(Of Integer)(&H30C)
    '=========== S64 - 8
    Dim playerLatitude As Offset(Of Long) = New FSUIPC.Offset(Of Long)(&H560)
    Dim playerLongitude As Offset(Of Long) = New FSUIPC.Offset(Of Long)(&H568)
    Dim altitude As Offset(Of Long) = New FSUIPC.Offset(Of Long)(&H570)
    '============ F64 - 9
    Dim currentweight As Offset(Of Double) = New FSUIPC.Offset(Of Double)(&H30C0)

    '============== cross function variable =====================
    Dim alt As Double
    Dim lat As Double
    Dim lon As Double
    Dim gndspeed As Double
    Dim stall As Byte = 0
    Dim ovs As Byte = 0
    Dim fstime As String
    Dim lastflaps As Integer = 0
    Dim onGround As Integer = 1
    Dim fuelweight As Integer
    Dim eng1 As Integer = 0
    Dim eng2 As Integer = 0
    Dim latArr As Double
    Dim lonArr As Double
    Dim latDep As Double
    Dim lonDep As Double
    Dim dist As Double
    Dim savedpark As Short = 32767
    Dim savedgear As Integer = 16383
    Dim savedNav As Byte = 0
    Dim savedStrobe As Byte = 0
    Dim savedLanding As Byte = 0
    Dim lastheading As UInteger = 370
    Dim savedxpdr As String = "0"
    Dim savedcom1 As String = "0"
    Dim savedcom2 As String = "0"
    Dim savednav1 As String = "0"
    Dim savednav2 As String = "0"
    Dim savedadf1 As String = "0"
    Dim savedadf2 As String = "0"
    Dim savedpush As String = "3"
    Dim savedairborne As UShort = "1"
    Dim flying As Integer = 0
    Dim flighttime As Integer = 0
    Dim timeremaining As String
    Dim distroute As String
    Dim roughest As Integer = 0
    Dim flightlog As String

    Private Function RadToDeg(ByVal radians As Double) As Double
        Return radians * (180 / Math.PI)
    End Function

    Private Function DegToRad(ByVal degrees As Double) As Double
        Return degrees / (180 / Math.PI)
    End Function

    Private Function Distance(ByVal lat1 As Double, ByVal lon1 As Double, ByVal lat2 As Double, ByVal lon2 As Double) As Double
        Return RadToDeg(Acos(Sin(DegToRad(lat1)) * Sin(DegToRad(lat2)) + Cos(DegToRad(lat1)) * Cos(DegToRad(lat2)) * Cos(DegToRad(lon1 - lon2)))) * 69.09D * 0.868976242D
    End Function

    Private Function GetTime(Time As Integer) As String
        Dim Hrs As Integer
        Dim Min As Integer
        Dim Sec As Integer

        Sec = Time Mod 60
        Min = ((Time - Sec) / 60) Mod 60
        Hrs = ((Time - (Sec + (Min * 60))) / 3600) Mod 60

        Return Format(Hrs, "00") & ":" & Format(Min, "00") & ":" & Format(Sec, "00")
    End Function

    Private Sub frmFlightLog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        
        lblDeparture.Text = dep & " - " & depfull
        lblArrival.Text = arr & " - " & arrfull
        lblAircraft.Text = acreg & " " & actype
        lblCruising.Text = cruising & " feet"
        lblRoute.Text = route
        lblFlightNum.Text = fltno

        lblGroundspeed.Text = ""
        lblLongitude.Text = ""
        lblLatitude.Text = ""
        lblPhase.Text = ""
        lblAltitude.Text = ""
        lblFlighttime.Text = ""
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        Try
            If conn.State <> ConnectionState.Open Then
                conn.Open()
            Else
                cmd = New MySqlCommand("SELECT lat, lng FROM phpvms_airports WHERE icao = '" & dep & "'", conn)
                result = cmd.ExecuteReader()
                result.Read()
                latDep = Double.Parse(result("lat").ToString)
                lonDep = Double.Parse(result("lng").ToString)
                result.Close()

                '================== Destination coordinate ================
                cmd = New MySqlCommand("SELECT lat, lng FROM phpvms_airports WHERE icao = '" & arr & "'", conn)
                result = cmd.ExecuteReader()
                result.Read()
                latArr = Double.Parse(result("lat").ToString)
                lonArr = Double.Parse(result("lng").ToString)
                result.Close()

                '========================================================================
                '============================== START FSUIPC ============================
                '========================================================================
                Try
                    FSUIPCConnection.Open()
                    FSUIPCConnection.Process()
                    lblStatus.Text = "FSUIPC Connected"
                    '==================== Aircraft position =================
                    gndspeed = (groundspeed.Value / 65536D) * (3600D / 1852D)
                    lblGroundspeed.Text = gndspeed.ToString("0") & " knots"

                    lat = playerLatitude.Value * 90D / (10001750D * 65536D * 65536D)
                    lon = playerLongitude.Value * 360D / (65536D * 65536D * 65536D * 65536D)
                    lblLatitude.Text = lat.ToString("0.000000")
                    lblLongitude.Text = lon.ToString("0.000000")

                    alt = altitude.Value * 3.28084D / (65536D * 65536D)
                    lblAltitude.Text = alt.ToString("0") & " feet"
                    lblPhase.Text = "Boarding"
                    If Distance(lat, lon, latDep, lonDep) > 5 Then
                        MessageBox.Show("Flightsim Departure Airport Not Match with Scheduled Departure")
                        FSUIPCConnection.Close()
                    ElseIf eng1stat.Value = 1 Then
                        MessageBox.Show("Engine 1 Running. Please Shutdown Engine Before Start")
                        FSUIPCConnection.Close()
                    ElseIf eng2stat.Value = 1 Then
                        MessageBox.Show("Engine 2 Running. Please Shutdown Engine Before Start")
                        FSUIPCConnection.Close()
                    ElseIf parkbrake.Value = 0 Then
                        MessageBox.Show("Parking Brake Not Set. Please Set Parking Brake Before Start")
                        FSUIPCConnection.Close()
                    Else
                        navlight.Value = 0
                        landinglight.Value = 0
                        strobelight.Value = 0
                        parkbrake.Value = 32767

                        tmrLog.Enabled = True
                        tmrLanding.Enabled = True
                        tmrACARS.Enabled = True
                        btnStart.Enabled = False
                        btnStop.Enabled = True
                        distroute = Distance(latDep, lonDep, latArr, lonArr).ToString("0")

                        '===================== Header Log =====================
                        deptime = DateTime.Now.ToString("HH:mm:ss")
                        txtLog.AppendText("Flight Log AirTrax " & Form5.version & Environment.NewLine)
                        txtLog.AppendText("=============================" & Environment.NewLine)
                        txtLog.AppendText("Flight Number " & fltno & Environment.NewLine)
                        txtLog.AppendText("Departure " & dep & Environment.NewLine)
                        txtLog.AppendText("Arrival " & arr & Environment.NewLine)
                        txtLog.AppendText(actype & " " & acreg & Environment.NewLine)
                        txtLog.AppendText("---------------------------------------------------" & Environment.NewLine)

                        '==================== Set time ====================
                        fstime = "[" & hours.Value.ToString("00") & ":" & mins.Value.ToString("00") & "] "
                        txtLog.AppendText(fstime & "Boarding" & Environment.NewLine)
                    End If
                Catch ex As Exception
                    MessageBox.Show("Cannot Connect To FSUIPC. Please Start Flight Simulator.")
                End Try
            End If
        Catch ex As Exception
            MessageBox.Show("Cannot Connect To Server. Please Try Again.")
        End Try
    End Sub

    Private Sub tmrLog_Tick(sender As Object, e As EventArgs) Handles tmrLog.Tick
        Try
            FSUIPCConnection.Process()
            '==================== Set time ====================
            fstime = "[" & hours.Value.ToString("00") & ":" & mins.Value.ToString("00") & "] "
            flighttime = flighttime + 1
            lblFlighttime.Text = GetTime(flighttime)

            '==================== Fuel weight ====================
            fuelweight = currentweight.Value - (zfw.Value \ 256D)

            '==================== Engine Start / Stop ================
            If eng1 = 0 And eng1stat.Value = 1 Then
                eng1 = 1
                txtLog.AppendText(fstime & "Engine 1 Started" & Environment.NewLine)
            ElseIf eng1 = 1 And eng1stat.Value = 0 Then
                eng1 = 0
                txtLog.AppendText(fstime & "Engine 1 Shutdown" & Environment.NewLine)
            End If
            If eng2 = 0 And eng2stat.Value = 1 Then
                eng2 = 1
                txtLog.AppendText(fstime & "Engine 2 Started" & Environment.NewLine)
            ElseIf eng2 = 1 And eng2stat.Value = 0 Then
                eng2 = 0
                txtLog.AppendText(fstime & "Engine 2 Shutdown" & Environment.NewLine)
            End If

            '==================== Aircraft position =================
            gndspeed = (groundspeed.Value / 65536D) * (3600D / 1852D)
            lblGroundspeed.Text = gndspeed.ToString("0") & " knots"

            lat = playerLatitude.Value * 90D / (10001750D * 65536D * 65536D)
            lon = playerLongitude.Value * 360D / (65536D * 65536D * 65536D * 65536D)
            lblLatitude.Text = lat.ToString("0.00000")
            lblLongitude.Text = lon.ToString("0.00000")

            alt = altitude.Value * 3.28084D / (65536D * 65536D)
            lblAltitude.Text = alt.ToString("0") & " feet"

            '====================== Distance to destination ========================
            dist = Distance(lat, lon, latArr, lonArr)
            If gndspeed > 1 Then
                timeremaining = GetTime(dist / (gndspeed / 3600D)).Substring(0, 5)
            Else
                timeremaining = "--:--"
            End If

            '======================= Landing Gear =================
            If savedgear = 0 And gear.Value = 16383 Then
                txtLog.AppendText(fstime & "Landing Gear Extended at " & alt.ToString("0") & " feet" & Environment.NewLine)
            ElseIf savedgear = 16383 And gear.Value = 0 Then
                txtLog.AppendText(fstime & "Landing Gear Retracted at " & alt.ToString("0") & " feet" & Environment.NewLine)
            End If
            savedgear = gear.Value

            '====================== Park Brake =====================
            If savedpark = 0 And parkbrake.Value = 32767 Then
                txtLog.AppendText(fstime & "Parking Brake ON" & Environment.NewLine)
            ElseIf savedpark = 32767 And parkbrake.Value = 0 Then
                txtLog.AppendText(fstime & "Parking Brake OFF" & Environment.NewLine)
            End If
            savedpark = parkbrake.Value

            '====================== Light Change =====================
            If savedNav = 0 And navlight.Value = 1 Then
                txtLog.AppendText(fstime & "NAV Light ON" & Environment.NewLine)
            ElseIf savedNav = 1 And navlight.Value = 0 Then
                txtLog.AppendText(fstime & "NAV Light OFF" & Environment.NewLine)
            End If
            savedNav = navlight.Value

            If savedStrobe = 0 And strobelight.Value = 1 Then
                txtLog.AppendText(fstime & "Beacon Light ON" & Environment.NewLine)
            ElseIf savedStrobe = 1 And strobelight.Value = 0 Then
                txtLog.AppendText(fstime & "Beacon Light OFF" & Environment.NewLine)
            End If
            savedStrobe = strobelight.Value

            If savedLanding = 0 And landinglight.Value = 1 Then
                txtLog.AppendText(fstime & "Landing Light ON" & Environment.NewLine)
            ElseIf savedLanding = 1 And landinglight.Value = 0 Then
                txtLog.AppendText(fstime & "Landing Light OFF" & Environment.NewLine)
            End If
            savedLanding = landinglight.Value

            '================== Stall warning =====================
            If stall = 0 And fsstall.Value = 1 Then
                txtLog.AppendText(fstime & "STALL Detected" & Environment.NewLine)
                stall = 1
            ElseIf stall = 1 And fsstall.Value = 0 Then
                txtLog.AppendText(fstime & "STALL Recovered" & Environment.NewLine)
                stall = 0
            End If
            '================= Overspeed warning ===================
            If ovs = 0 And fsover.Value = 1 Then
                txtLog.AppendText(fstime & "OVERSPEED Detected" & Environment.NewLine)
                ovs = 1
            ElseIf ovs = 1 And fsover.Value = 0 Then
                txtLog.AppendText(fstime & "OVERSPEED Recovered" & Environment.NewLine)
                ovs = 0
            End If

            '=================== Pushback status ====================
            If opr <> "Operated by NAM Flying School" Then
                If savedpush = "3" And pushback.Value < 3 Then
                    txtLog.AppendText(fstime & "Pushing Back" & Environment.NewLine)
                    lblPhase.Text = "Pushing Back"
                ElseIf gndspeed < 1 And Integer.Parse(savedpush) < 3 And pushback.Value = 3 Then
                    lblPhase.Text = "Standby"
                End If
                savedpush = pushback.Value.ToString
            End If

            '================ Flight status ===================
            If gndspeed > 2 And gndspeed < 30 And savedairborne = "1" And lblPhase.Text <> "Taxiing" Then
                txtLog.AppendText(fstime & "Taxiing" & Environment.NewLine)
                lblPhase.Text = "Taxiing"
            ElseIf gndspeed >= 30 And lblPhase.Text = "Taxiing" And savedairborne = "1" Then
                txtLog.AppendText(fstime & "Taking Off" & Environment.NewLine)
                lblPhase.Text = "Taking Off"
            ElseIf flying = 1 And gndspeed = 0 And savedpark = 32767 And lblPhase.Text = "Taxiing" And eng1 = 0 Then
                txtLog.AppendText(fstime & "On Blocks" & Environment.NewLine)
                If Distance(lat, lon, latArr, lonArr) > 5 Then
                    lblPhase.Text = "Diverted"
                Else
                    lblPhase.Text = "Arrived"
                End If
            End If

            Dim ground As Integer = Convert.ToInt32(elevation.Value * 3.28084D)
            If opr = "Operated by NAM Flying School" Then
                If alt - ground > 500 And alt < Integer.Parse(cruising) - 500 And lblPhase.Text = "Airborne" Then
                    txtLog.AppendText(fstime & "Climbing" & Environment.NewLine)
                    lblPhase.Text = "Climbing"
                ElseIf lblPhase.Text = "Climbing" And (alt > Integer.Parse(cruising) - 500 Or alt >= Integer.Parse(cruising)) Then
                    txtLog.AppendText(fstime & "Cruising" & Environment.NewLine)
                    lblPhase.Text = "Cruising"
                ElseIf lblPhase.Text = "Cruising" And alt - ground < Integer.Parse(cruising) - 500 Then
                    txtLog.AppendText(fstime & "Initial Approach" & Environment.NewLine)
                    lblPhase.Text = "Initial Approach"
                ElseIf lblPhase.Text = "Initial Approach" And alt - ground < 700 Then
                    txtLog.AppendText(fstime & "Final Approach" & Environment.NewLine)
                    lblPhase.Text = "Final Approach"
                End If
            Else
                If alt - ground > 1500 And alt < Integer.Parse(cruising) - 1000 And lblPhase.Text = "Airborne" Then
                    txtLog.AppendText(fstime & "Climbing" & Environment.NewLine)
                    lblPhase.Text = "Climbing"
                ElseIf lblPhase.Text = "Climbing" And (alt > Integer.Parse(cruising) - 1000 Or alt >= Integer.Parse(cruising)) Then
                    txtLog.AppendText(fstime & "Cruising" & Environment.NewLine)
                    lblPhase.Text = "Cruising"
                ElseIf lblPhase.Text = "Cruising" And (alt < Integer.Parse(cruising) - 3000 And alt - ground >= 5000) Then
                    txtLog.AppendText(fstime & "Descending" & Environment.NewLine)
                    lblPhase.Text = "Descending"
                ElseIf lblPhase.Text = "Descending" And alt - ground < 5000 Then
                    txtLog.AppendText(fstime & "Initial Approach" & Environment.NewLine)
                    lblPhase.Text = "Initial Approach"
                ElseIf lblPhase.Text = "Initial Approach" And alt - ground < 700 Then
                    txtLog.AppendText(fstime & "Final Approach" & Environment.NewLine)
                    lblPhase.Text = "Final Approach"
                End If
            End If

            '=================== Flaps Detection =====================
            If lastflaps <> flaps.Value Then
                lastflaps = flaps.Value
                If opr = "Operated by NAM Flying School" Then '= Piper
                    If onGround = 1 Then
                        Select Case flaps.Value
                            Case 0
                                txtLog.AppendText(fstime & "Flaps 0" & Environment.NewLine)
                            Case 5460
                                txtLog.AppendText(fstime & "Flaps 10" & Environment.NewLine)
                            Case 10922
                                txtLog.AppendText(fstime & "Flaps 25" & Environment.NewLine)
                            Case 16383
                                txtLog.AppendText(fstime & "Flaps 40" & Environment.NewLine)
                        End Select
                    Else
                        Select Case flaps.Value
                            Case 0
                                txtLog.AppendText(fstime & "Flaps 0 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 5460
                                txtLog.AppendText(fstime & "Flaps 10 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 10922
                                txtLog.AppendText(fstime & "Flaps 25 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 16383
                                txtLog.AppendText(fstime & "Flaps 40 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                        End Select
                    End If
                Else '= Boeing
                    If onGround = 1 Then
                        Select Case flaps.Value
                            Case 0
                                txtLog.AppendText(fstime & "Flaps 0" & Environment.NewLine)
                            Case 2047
                                txtLog.AppendText(fstime & "Flaps 1" & Environment.NewLine)
                            Case 4095
                                txtLog.AppendText(fstime & "Flaps 2" & Environment.NewLine)
                            Case 6143
                                txtLog.AppendText(fstime & "Flaps 5" & Environment.NewLine)
                            Case 8191
                                txtLog.AppendText(fstime & "Flaps 10" & Environment.NewLine)
                            Case 10239
                                txtLog.AppendText(fstime & "Flaps 15" & Environment.NewLine)
                            Case 12287
                                txtLog.AppendText(fstime & "Flaps 25" & Environment.NewLine)
                            Case 14335
                                txtLog.AppendText(fstime & "Flaps 30" & Environment.NewLine)
                            Case 16383
                                txtLog.AppendText(fstime & "Flaps 40" & Environment.NewLine)
                        End Select
                    Else
                        Select Case flaps.Value
                            Case 0
                                txtLog.AppendText(fstime & "Flaps 0 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 2047
                                txtLog.AppendText(fstime & "Flaps 1 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 4095
                                txtLog.AppendText(fstime & "Flaps 2 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 6143
                                txtLog.AppendText(fstime & "Flaps 5 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 8191
                                txtLog.AppendText(fstime & "Flaps 10 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 10239
                                txtLog.AppendText(fstime & "Flaps 15 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 12287
                                txtLog.AppendText(fstime & "Flaps 25 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 14335
                                txtLog.AppendText(fstime & "Flaps 30 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                            Case 16383
                                txtLog.AppendText(fstime & "Flaps 40 at " & gndspeed.ToString("0") & " kts and " & alt.ToString("0") & " ft" & Environment.NewLine)
                        End Select
                    End If
                End If
            End If

            '=================== Coordinate Logging ===================
            Dim hdg As Integer = heading.Value * 360D / (65536D * 65536D)
            If lastheading < hdg - 5 Or lastheading > hdg + 5 Then
                txtCoordLog.AppendText("*" & lat.ToString("0.0000") & ", " & lon.ToString("0.0000"))
                lastheading = hdg
            End If

            '================= Frequency Change ====================
            If savedxpdr <> Hex(xpdr.Value) Then
                tmrAttitude.Enabled = True
            End If

            If savedcom1 <> Hex(com1.Value) Then
                tmrAttitude.Enabled = True
            End If

            If savedcom2 <> Hex(com2.Value) Then
                tmrAttitude.Enabled = True
            End If

            If savednav1 <> Hex(nav1.Value) Then
                tmrAttitude.Enabled = True
            End If

            If savednav2 <> Hex(nav2.Value) Then
                tmrAttitude.Enabled = True
            End If

            If savedadf1 <> Hex(adf1ext.Value) & Hex(adf1.Value) Then
                tmrAttitude.Enabled = True
            End If

            If savedadf2 <> Hex(adf2ext.Value) & Hex(adf2.Value) Then
                tmrAttitude.Enabled = True
            End If
        Catch ex As Exception
            lblStatus.Text = "FSUIPC Disconnected. Verify Flightsim is Running."
        End Try
    End Sub

    Private Sub tmrLanding_Tick(sender As Object, e As EventArgs) Handles tmrLanding.Tick
        Try
            FSUIPCConnection.Process()
            '============== Disable pause, slew, and simrate ==============
            pause.Value = 0
            If slewMode.Value = 1 Then
                slewMode.Value = 0
            End If
            simrate.Value = 256

            '================== Touchdown V/S =====================
            Dim touchdown As Integer = Convert.ToInt32(tdvs.Value * 60D * 3.28084D / 256D)

            '==================== Airborne =====================
            If savedairborne = 1 And airborne.Value = 0 Then
                txtLog.AppendText(fstime & "Airborne with " & fuelweight.ToString & " lbs of fuel" & Environment.NewLine)
                txtLog.AppendText(fstime & "Rotate at " & gndspeed.ToString("0") & " kts" & Environment.NewLine)
                lblPhase.Text = "Airborne"
                flying = 1
                onGround = 0
            ElseIf savedairborne = 0 And airborne.Value = 1 Then
                If touchdown < -1000 Then
                    tmrLanding.Enabled = False
                    tmrACARS.Enabled = False
                    tmrAttitude.Enabled = False
                    tmrLog.Enabled = False
                    MessageBox.Show("Landing rate worse than -1000 fpm. PIREP automatically rejected. AirTrax will be closed.")
                    Application.Exit()
                Else
                    txtLog.AppendText(fstime & "Landed with " & fuelweight.ToString & " lbs of fuel" & Environment.NewLine)
                    txtLog.AppendText(fstime & "Touchdown rate " & touchdown.ToString & " fpm at " & gndspeed.ToString("0") & " kts" & Environment.NewLine)
                    lblPhase.Text = "Landed"
                    onGround = 1
                    If touchdown < roughest Then
                        roughest = touchdown
                    End If
                End If
            End If
            savedairborne = airborne.Value

        Catch ex As Exception

        End Try
        End Sub

    Private Sub tmrAttitude_Tick(sender As Object, e As EventArgs) Handles tmrAttitude.Tick
        Try
            '==================== Transponder =================
            If savedxpdr <> Hex(xpdr.Value) Then
                Dim squawk As String
                If Integer.Parse(Hex(xpdr.Value)) < 1000 Then
                    squawk = "0" & Hex(xpdr.Value)
                Else
                    squawk = Hex(xpdr.Value)
                End If
                txtLog.AppendText(fstime & "Transponder set to " & squawk & "" & Environment.NewLine)
                savedxpdr = Hex(xpdr.Value)
            End If

            '======================= COM1 =================
            If savedcom1 <> Hex(com1.Value) Then
                Dim com As String = "1" & Hex(com1.Value).Substring(0, 2) & "." & Hex(com1.Value).Substring(2, 2)
                txtLog.AppendText(fstime & "COM1 set to " & com & "" & Environment.NewLine)
                savedcom1 = Hex(com1.Value)
            End If

            '======================= COM2 =================
            If savedcom2 <> Hex(com2.Value) Then
                Dim com As String = "1" & Hex(com2.Value).Substring(0, 2) & "." & Hex(com2.Value).Substring(2, 2)
                txtLog.AppendText(fstime & "COM2 set to " & com & "" & Environment.NewLine)
                savedcom2 = Hex(com2.Value)
            End If

            '======================= NAV1 =================
            If savednav1 <> Hex(nav1.Value) Then
                Dim nav As String = "1" & Hex(nav1.Value).Substring(0, 2) & "." & Hex(nav1.Value).Substring(2, 2)
                txtLog.AppendText(fstime & "NAV1 set to " & nav & "" & Environment.NewLine)
                savednav1 = Hex(nav1.Value)
            End If

            '======================= NAV2 =================
            If savednav2 <> Hex(nav2.Value) Then
                Dim nav As String = "1" & Hex(nav2.Value).Substring(0, 2) & "." & Hex(nav2.Value).Substring(2, 2)
                txtLog.AppendText(fstime & "NAV2 set to " & nav & "" & Environment.NewLine)
                savednav2 = Hex(nav2.Value)
            End If

            '======================= ADF1 =================
            If savedadf1 <> Hex(adf1ext.Value) & Hex(adf1.Value) Then
                Dim adf As String
                If Integer.Parse(adf1ext.Value) > 99 Then
                    adf = Hex(adf1ext.Value).Substring(0, 1) & Hex(adf1.Value) & "." & Hex(adf1ext.Value).Substring(2, 1)
                Else
                    adf = Hex(adf1.Value) & "." & Hex(adf1ext.Value)
                End If
                txtLog.AppendText(fstime & "ADF1 set to " & adf & "" & Environment.NewLine)
                savedadf1 = Hex(adf1ext.Value) & Hex(adf1.Value)
            End If

            '======================= ADF2 =================
            If opr <> "Operated by NAM Flying School" Then
                If savedadf2 <> Hex(adf2ext.Value) & Hex(adf2.Value) Then
                    Dim adf As String
                    If Integer.Parse(adf2ext.Value) > 99 Then
                        adf = Hex(adf2ext.Value).Substring(0, 1) & Hex(adf2.Value) & "." & Hex(adf2ext.Value).Substring(2, 1)
                    Else
                        adf = Hex(adf2.Value) & "." & Hex(adf2ext.Value)
                    End If
                    txtLog.AppendText(fstime & "ADF2 set to " & adf & "" & Environment.NewLine)
                    savedadf2 = Hex(adf2ext.Value) & Hex(adf2.Value)
                End If
            End If

            tmrAttitude.Enabled = False
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tmrACARS_Tick(sender As Object, e As EventArgs) Handles tmrACARS.Tick
        If Not BackgroundWorker1.IsBusy Then
            BackgroundWorker1.RunWorkerAsync()
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            If conn.State <> ConnectionState.Open Then
                conn.Open()
            Else
                If (acarsavailable = 0) Then
                    cmd = New MySqlCommand("SELECT * FROM phpvms_acarsdata WHERE pilotid = '" & pilotid & "'", conn)
                    result = cmd.ExecuteReader
                    result.Read()
                    If result.HasRows() Then
                        result.Close()
                        acarsavailable = 1
                    Else
                        result.Close()
                        Dim cmd1 As MySqlCommand = New MySqlCommand("INSERT INTO phpvms_acarsdata (pilotid, pilotname) VALUES ('" & pilotid & "', '" & pilotname & "')", conn)
                        cmd1.ExecuteScalar()
                    End If
                Else
                    cmd = New MySqlCommand("UPDATE phpvms_acarsdata SET flightnum = '" & fltno & "', aircraft = '" & acreg & "', lat = '" & lblLatitude.Text & "', lng = '" & lblLongitude.Text & "', heading = '" & lastheading & "', alt = '" & alt.ToString("0") & "', gs = '" & gndspeed.ToString & "', depicao = '" & dep & "', arricao = '" & arr & "', deptime = '" & deptime & "', timeremaining = '" & timeremaining & "', route = '" & route & "', route_details = '" & opr & "', distremain = '" & dist.ToString("0") & "', phasedetail = '" & lblPhase.Text & "', lastupdate = '" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") & "', client = 'AirTrax 3' WHERE pilotid = '" & pilotid & "'", conn)
                    cmd.ExecuteScalar()
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        If flying = 1 Then
            FSUIPCConnection.Process()
            If parkbrake.Value = 0 Then
                MessageBox.Show("Parking Brake Not Set. Please Set Parking Brake Before Stop")
            ElseIf eng1 = 1 Then
                MessageBox.Show("Engine 1 Running. Please Shutdown Engine Before Stop")
            ElseIf eng2 = 1 Then
                MessageBox.Show("Engine 2 Running. Please Shutdown Engine Before Stop")
            Else
                flightlog = txtLog.Text.Replace(Environment.NewLine, "*")
                tmrAttitude.Enabled = False
                tmrLanding.Enabled = False
                tmrLog.Enabled = False
                arrtime = DateTime.Now.ToString("HH:mm:ss")
                btnStop.Enabled = False
                btnSubmit.Enabled = True

                '======================= Write backup file ===================
                Dim filepath As String = "FlightPath.atx"
                If File.Exists(filepath) Then
                    My.Computer.FileSystem.DeleteFile(filepath)
                End If
                Dim sw As StreamWriter = File.CreateText(filepath)
                sw.WriteLine(txtCoordLog.Text)
                sw.Flush()
                sw.Close()

                Dim filelog As String = "FlightLog.atx"
                If File.Exists(filelog) Then
                    My.Computer.FileSystem.DeleteFile(filelog)
                End If
                Dim sw1 As StreamWriter = File.CreateText(filelog)
                sw1.WriteLine(flightlog)
                sw1.Flush()
                sw1.Close()
            End If
        Else
            MessageBox.Show("Flight Not Flown. Cannot Stop Flight.")
        End If
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim submitdate As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            Dim accepted As String
            If lblPhase.Text = "Arrived" Then
                accepted = "1"
            Else
                accepted = "0"
            End If
            Dim pireptime As String = lblFlighttime.Text.Substring(0, 2) & "." & lblFlighttime.Text.Substring(3, 2)
            cmd = New MySqlCommand("INSERT INTO phpvms_pireps (pilotid, flightnum, depicao, arricao, route, aircraft, flighttime, flighttime_stamp, distance, landingrate, submitdate, accepted, log, flighttype, source) VALUES ('" & pilotid & "', '" & fltno & "', '" & dep & "', '" & arr & "', '" & route & "', '" & ac_id & "', '" & pireptime & "', '" & lblFlighttime.Text & "', '" & distroute & "', '" & roughest.ToString & "', '" & submitdate & "', '" & accepted & "', '" & flightlog & "', 'P', 'AirTrax 3')", conn)
            cmd.ExecuteScalar()

            cmd = New MySqlCommand("SELECT pirepid FROM phpvms_pireps WHERE submitdate = '" & submitdate & "'", conn)
            result = cmd.ExecuteReader()
            result.Read()
            Dim pirepid As String = result("pirepid").ToString
            result.Close()

            If String.IsNullOrEmpty(txtComment.Text) = False Then
                cmd = New MySqlCommand("INSERT INTO phpvms_pirepcomments (pirepid, pilotid, comment, postdate) VALUES ('" & pirepid & "', '" & pilotid & "', '" & txtComment.Text & "', '" & submitdate & "')", conn)
                cmd.ExecuteScalar()
            End If

            cmd = New MySqlCommand("UPDATE phpvms_pilots SET totalflights = totalflights + 1, totalhours = totalhours + '" & pireptime & "', lastpirep = '" & submitdate & "' WHERE pilotid = '" & pilotid & "'", conn)
            cmd.ExecuteScalar()

            cmd = New MySqlCommand("INSERT INTO airtrax_pirep (pirepid, coordinate, operator) VALUES ('" & pirepid & "', '" & txtCoordLog.Text & "', '" & opr & "')", conn)
            cmd.ExecuteScalar()

            If bidid <> "x" Then
                cmd = New MySqlCommand("UPDATE phpvms_schedules SET bidid = 0 WHERE id = '" & routeid & "'", conn)
                cmd.ExecuteScalar()

                cmd = New MySqlCommand("DELETE FROM phpvms_bids WHERE bidid = '" & bidid & "'", conn)
                cmd.ExecuteScalar()
            End If
            MessageBox.Show("PIREP Filed. Thank You For Flying.")
            Application.ExitThread()
        Catch ex As Exception
            MessageBox.Show("Failed to Send PIREP. Please try again.")
        End Try
    End Sub

    Public Sub New()
        Dim myCi = New CultureInfo("en-US", False)
        myCi.NumberFormat.CurrencyDecimalSeparator = "."
        myCi.NumberFormat.NumberDecimalSeparator = "."
        myCi.NumberFormat.CurrencyGroupSeparator = ","
        myCi.NumberFormat.NumberGroupSeparator = ","
        myCi.NumberFormat.PercentDecimalSeparator = "."
        myCi.NumberFormat.PercentGroupSeparator = ","

        Thread.CurrentThread.CurrentCulture = myCi
        Thread.CurrentThread.CurrentUICulture = myCi

        InitializeComponent()
    End Sub

    Private Sub frmFlightLog_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Application.ExitThread()
    End Sub
End Class